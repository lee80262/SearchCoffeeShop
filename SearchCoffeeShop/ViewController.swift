//
//  ViewController.swift
//  SearchCoffeeShop
//
//  Created by Lee on 2022/6/2.
//

import UIKit
import MapKit

class ViewController: UIViewController {
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var cityLabel: UILabel!
	@IBOutlet weak var addressLabel: UILabel!
	@IBOutlet weak var urlButton: UIButton!
	@IBOutlet weak var mapView: MKMapView!
	
	let cafe: CoffeeData
	
	init?(coder: NSCoder, cafe: CoffeeData) {
		self.cafe = cafe
		super.init(coder: coder)
	}
	
	required init?(coder: NSCoder) {
		fatalError("(init(coder:) has not been implemented")
	}
	
	@IBSegueAction func showWeb(_ coder: NSCoder) -> WebViewController? {
		let controller = WebViewController(coder: coder)
		if let url = cafe.url {
			controller?.url = url
		} else {
			return nil
		}
		return controller
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// 文字設置
		nameLabel.text = cafe.name
		cityLabel.text = "城市: \(cafe.city)"
		addressLabel.text = cafe.address
		urlButton.contentHorizontalAlignment = .left
		urlButton.setTitle("臉書連結", for: .normal)
		
		// 自動換行
		nameLabel.numberOfLines = 0
		
		// 地圖設置
		let initLocation = CLLocationCoordinate2D(latitude: Double(cafe.latitude)!, longitude: Double(cafe.longitude)!)
		let region = MKCoordinateRegion(center: initLocation, latitudinalMeters: 250, longitudinalMeters: 250)
		let annotation = MKPointAnnotation()
		annotation.title = cafe.name
		annotation.coordinate = initLocation
		mapView.setCenter(initLocation, animated: true)
		mapView.setRegion(region, animated: true)
		mapView.addAnnotation(annotation)
	}
}
