//
//  CoffeeTableViewController.swift
//  SearchCoffeeShop
//
//  Created by Lee on 2022/6/8.
//

import UIKit

class CoffeeTableViewController: UITableViewController {
	
	@IBOutlet weak var searchBar: UISearchBar!
	
	var cafes = [CoffeeData]()
	var searchCafes = [CoffeeData]()
	var searching = false

    override func viewDidLoad() {
        super.viewDidLoad()
        getCoffeeData()
		searchBar.delegate = self
    }
	
	// 取得API資料
	func getCoffeeData() {
		let urlStr = "https://cafenomad.tw/api/v1.2/cafes"
		if let url = URL(string: urlStr) {
			let decoder = JSONDecoder()
			let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
				if let data = data {
					do {
						let coffeeResult = try decoder.decode([CoffeeData].self, from: data)
						self.cafes = coffeeResult
						DispatchQueue.main.async {
							self.tableView.reloadData()
						}
					} catch {
						print("error")
					}
				}
			})
			task.resume()
		}
	}
	
	@IBSegueAction func showDetail(_ coder: NSCoder) -> ViewController? {
		// 下面這句是為了確保有選到row，否則回傳nil
		guard let row = tableView.indexPathForSelectedRow?.row else { return nil }
			
		// 選到row之後回傳該row對應的咖啡廳資訊頁
		if searching {
			return ViewController(coder: coder, cafe: searchCafes[row])
		} else {
			return ViewController(coder: coder, cafe: cafes[row])
		}
	}
	
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, re(turn the number of rows
		if searching {
			return searchCafes.count
		} else {
			return cafes.count
		}
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		
		var cafe = CoffeeData(name: "", city: "", address: "", latitude: "", longitude: "")
		if searching {
			cafe = searchCafes[indexPath.row]
		} else {
			cafe = cafes[indexPath.row]
		}
		cell.textLabel?.text = cafe.name
		cell.textLabel?.textColor = UIColor.systemGreen
        return cell
    }
}

extension CoffeeTableViewController: UISearchBarDelegate {
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		searchCafes = cafes.filter( { $0.name.lowercased().prefix(searchText.count) == searchText.lowercased() })
		searching = true
		tableView.reloadData()
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searching = false
		searchBar.text = ""
		searchBar.resignFirstResponder()
		tableView.reloadData()
	}
}

