//
//  WebViewController.swift
//  SearchCoffeeShop
//
//  Created by Lee on 2022/6/8.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

	@IBOutlet weak var webView: WKWebView!
	
	var url = String()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		if let url = URL(string: url) {
			let request = URLRequest(url: url)
			webView.load(request)
		}
        
    }
    

}
