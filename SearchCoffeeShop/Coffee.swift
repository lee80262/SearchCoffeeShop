//
//  Coffee.swift
//  SearchCoffeeShop
//
//  Created by Lee on 2022/6/8.
//

import Foundation

struct CoffeeData: Codable {
	var name: String
	var city: String
	var url: String?
	var address: String
	var latitude: String
	var longitude: String
}
